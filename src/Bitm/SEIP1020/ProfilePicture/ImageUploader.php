<?php
namespace App\Bitm\SEIP1020\ProfilePicture;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;
use PDO;
use PDOException;



class ImageUploader{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn;
    public $username="root";
    public $password="";


    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=localhost;dbname=atomicprojectb22", $this->username, $this->password);

        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return  $this;

    }

    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`profilepicture` (`name`, `images`) VALUES (:name,:images)";
        $result=$this->conn->prepare($query);
        $result->execute(array(':name'=>$this->name,':images'=>$this->image_name));
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function index(){
        $_allinfo=array();
        $query="SELECT * FROM `atomicprojectb22`.`profilepicture`";
        $result=$this->conn->query($query);
        $_allinfo=$result->fetchAll(PDO::FETCH_OBJ);

        return $_allinfo;

    }
    public function view(){
        $query="SELECT * FROM `atomicprojectb22`.`profilepicture` WHERE `id`=:id";
        $result=$this->conn->prepare($query);
        $result->execute(array(':id'=>$this->id));
        $row=$result->fetch(PDO::FETCH_OBJ);
        return $row;
    }


    public function update(){
        if(!empty($this->image_name)) {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = :name, `images` = :images WHERE `profilepicture`.`id` =:id";
            $result=$this->conn->prepare($query);
            $result->execute(array(':name'=>$this->name,':images'=>$this->image_name,':id'=>$this->id));

        }
        else {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = :name WHERE `profilepicture`.`id` =:id";
            $result=$this->conn->prepare($query);
            $result->execute(array(':name'=>$this->name,':id'=>$this->id));

        }

        //$result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }


    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`profilepicture`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }




}



?>



